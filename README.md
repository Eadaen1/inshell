Clean room reimplementation of shell-pype.

### Usage

Use py "<some_code_here>".

    cat foo.txt | py '[print(x.split(" ")) for x in  IN]' > bar.txt


### Licence

Licenced under MIT Licence.
