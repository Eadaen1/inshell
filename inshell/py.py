import sys
import os
import csv
import json


_CODE = sys.argv[1]
IN = []
if len(sys.argv) > 2:
    IN += sys.argv[2:]
else:
    IN += [x[:-1] for x in sys.stdin]


def main():
    exec(_CODE)


if __name__ == "__main__":
    main()
