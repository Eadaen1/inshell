from setuptools import setup, find_packages


__author__ = "Eadaen <eadaen@protonmail.com>"


with open("README.md") as readme_file:
    long_description = readme_file.read()


setup(name='inshell',
      version='0.0.1',
      author='Eadaen',
      author_email='eadaen@protonmail.com',
      url='',
      description='Run Python easily in shell.',
      entry_points={
          "console_scripts": [
              "py = inshell.py:main"
          ]
      },
      long_description=long_description,
      long_description_content_type="text/markdown",
      license='MIT License',
      packages=find_packages(), )
